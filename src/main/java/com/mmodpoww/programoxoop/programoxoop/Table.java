/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mmodpoww.programoxoop.programoxoop;

/**
 *
 * @author Acer
 */
public class Table {

    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player playerX;
    private Player playerO;
    private Player currentPlayer;
    private Player winner;
    private boolean finish = false;
    private int lastcol;
    private int lastrow;
    private int countturn = 0;
    public Table(Player x, Player o) {
        playerX = x;
        playerO = o;
        currentPlayer = x;
    }

    public void showTable() {
        System.out.println(" 123");
        for (int row = 0; row < table.length; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < table[row].length; col++) {
                System.out.print(table[row][col]);
            }
            System.out.println("");
        }
    }

    public boolean setRowCol(int row, int col) {
        if (table[row][col] == '-') {
            table[row][col] = currentPlayer.getName();
            this.lastrow = row;
            this.lastcol = col;
            return true;
        }
        return false;
    }
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    public void switchPlayer(){
        if(currentPlayer == playerX){
            currentPlayer = playerO;
        }else{
            currentPlayer = playerX;
        }
        countturn++;
    }
    void checkCol() {
        for (int row = 0; row < 3; row++) {
            if (table[row][lastcol] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    void checkRow() {
        for (int col = 0; col < 3; col++) {
            if (table[lastrow][col] != currentPlayer.getName()) {
                return;
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    private void setStatWinLose() {
        if(currentPlayer == playerO){
            playerO.win();
            playerX.lose();
        }else{
            playerO.lose();
            playerX.win();
        }
    }

    void checkX() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (table[j][j] != currentPlayer.getName()) {
                    return;
                }
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }
    void checkY(){
        for (int i = 0; i < 3; i++) {
            for (int j = 0, k = 2; j < 3; j++, k--) {
                if (table[j][k] != currentPlayer.getName()) {
                    return;
                }
            }
        }
        finish = true;
        winner = currentPlayer;
        setStatWinLose();
    }

    

    void checkDraw() {
        if (countturn >= 8) {
            finish = true;
            playerO.draw();
            playerX.draw();
        }

    }
    public void checkWin() {
        checkRow();
        checkCol();
        checkX();
        checkY();
        checkDraw();
    }
    public boolean isFinish(){
        return finish;
    }
    public Player getWinner(){
        return winner;
    }
}
